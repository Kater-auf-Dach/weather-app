import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:weather_app/utils/theme_changer.dart';

import 'package:weather_app/widgets/city_view.dart';
import 'package:weather_app/widgets/detail_view.dart';
import 'package:weather_app/widgets/icon_vew.dart';
import 'package:weather_app/api/weather_api.dart';
import 'package:weather_app/models/weather_current.dart';
import 'package:weather_app/utils/location.dart';
import 'package:weather_app/utils/shared_prefs.dart';

class WeatherForecastScreen extends StatefulWidget {
  WeatherForecastScreen();

  @override
  _WeatherForecastScreenState createState() => _WeatherForecastScreenState();
}

// TODO: вся логика в одном классе
class _WeatherForecastScreenState extends State<WeatherForecastScreen> {
  Future<WeatherCurrent> forecastObject;
  Position position;
  bool isLoading = true;
  String latitude;
  String longitude;

  @override
  void initState() {
    super.initState();

    if (sharedPrefs.cityName != '') {
      _getDataByCity(sharedPrefs.cityName);
    } else {
      _getDataByLastLocation();
    }
  }

  @override
  Widget build(BuildContext context) {
    var themeChanger = Provider.of<ThemeChanger>(context);

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: FutureBuilder<WeatherCurrent>(
        future: forecastObject,
        builder: (context, snapshot) {
          if (isLoading) {
            return Center(
              child: SpinKitDoubleBounce(
                color: Theme.of(context).accentColor,
                size: 100,
              ),
            );
          } else if (snapshot.hasData) {
            return Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    IconButton(
                        icon:
                            Icon(IconData(59458, fontFamily: 'MaterialIcons')),
                        iconSize: 30,
                        color: Theme.of(context).accentColor,
                        onPressed: () {
                          setState(() {
                            isLoading = true;
                          });
                          _getDataByCurrentLocation();
                        }),
                    IconButton(
                        icon: _getIcon(themeChanger.themeName),
                        iconSize: 34,
                        color: Theme.of(context).accentColor,
                        onPressed: () {
                          themeChanger.toggleTheme();
                        }),
                  ],
                ),
                Expanded(
                    child: Flex(
                  direction: Axis.vertical,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CityView(snapshot: snapshot),
                    IconView(snapshot: snapshot),
                    DetailView(snapshot: snapshot),
                  ],
                ))
              ],
            );
          } else {
            return Center(
              child: SpinKitDoubleBounce(
                color: Colors.black87,
                size: 100,
              ),
            );
          }
        },
      ),
    );
  }

  _getDataByCity(cityName) async {
    var weatherInfo =
        await WeatherApi().fetchWeatherWithCity(cityName: cityName);

    setState(() {
      isLoading = true;

      if (weatherInfo != null) {
        forecastObject = Future.value(weatherInfo);
        isLoading = false;
      } else {
        _getDataByLastLocation();
      }
    });
  }

  _getDataByLastLocation() async {
    position = await Location().getLastKnownLocation();

    if (position != null) {
      latitude = position.latitude.toString();
      longitude = position.longitude.toString();

      var weatherInfo = await WeatherApi()
          .fetchWeatherWithLocation(latitude: latitude, longitude: longitude);

      setState(() {
        isLoading = true;

        if (weatherInfo != null) {
          forecastObject = Future.value(weatherInfo);
          isLoading = false;
        } else {
          Navigator.pushNamed(context, '/city');
        }
      });

      _getDataByCurrentLocation();
    } else {
      _getDataByCurrentLocation();
    }
  }

  _getDataByCurrentLocation() async {
    position = await Location().getCurrentLocation();

    if (position != null) {
      if (latitude == position.latitude.toString() &&
          longitude == position.longitude.toString()) {
        return;
      }

      latitude = position.latitude.toString();
      longitude = position.longitude.toString();

      var weatherInfo = await WeatherApi()
          .fetchWeatherWithLocation(latitude: latitude, longitude: longitude);

      setState(() {
        isLoading = true;

        if (weatherInfo != null) {
          forecastObject = Future.value(weatherInfo);
          isLoading = false;
        } else {
          Navigator.pushNamed(context, '/city');
        }
      });
      _saveCityFromLocation();
    } else if (sharedPrefs.cityName == null) {
      Navigator.pushNamed(context, '/city');
    } else {
      _getDataByCity(sharedPrefs.cityName);
    }
  }

  _saveCityFromLocation() async {
    var cityName = await WeatherApi()
        .getCityNameFromLocation(latitude: latitude, longitude: longitude);
    sharedPrefs.cityName = cityName;
  }

  _getIcon(themeName) {
    if (themeName == 'dark') {
      return Icon(Icons.brightness_6);
    } else if (themeName == 'light') {
      return Icon(Icons.brightness_6_outlined);
    } else if (themeName == 'auto-light') {
      return Icon(Icons.brightness_auto_sharp);
    } else {
      return Icon(Icons.brightness_auto_outlined);
    }
  }
}
