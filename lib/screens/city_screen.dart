import 'package:flutter/material.dart';
// import 'package:weather_app/api/address_api.dart';
import 'package:weather_app/models/address-result.dart';
import 'package:weather_app/utils/shared_prefs.dart';

class CityScreen extends StatefulWidget {
  final cityName;

  CityScreen({this.cityName});

  @override
  _CityScreenState createState() => _CityScreenState();
}

class _CityScreenState extends State<CityScreen> {
  String cityName;
  Future<List<AddressResult>> result;

  void _saveCity() async {
    sharedPrefs.cityName = cityName;
    await Navigator.pushNamed(context, '/home');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme
          .of(context)
          .backgroundColor,
      body: Container(
          child: SafeArea(
            child: Center(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Center(
                      child: Text(
                          'Введите город',
                          style: TextStyle(
                              color: Theme
                                  .of(context)
                                  .accentColor,
                              fontSize: 18
                          )
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: TextField(
                        autofocus: true,
                        textAlign: TextAlign.center,
                        textCapitalization: TextCapitalization.sentences,
                        cursorColor: Theme
                            .of(context)
                            .primaryColor,
                        style: TextStyle(color: Theme
                            .of(context)
                            .primaryColor, fontSize: 22),
                        onChanged: (value) async {
                          cityName = value;
                          // var res = await AddressApi().getCities(value);
                          // setState(() {
                          //   result = Future.value(res);
                          // });
                        },
                      ),
                    ),
                    FlatButton(
                      child: Text(
                          'Сохранить',
                          style: TextStyle(
                              color: Theme
                                  .of(context)
                                  .primaryColor,
                              fontSize: 22
                          )
                      ),
                      onPressed: _saveCity,
                      color: Colors.transparent,
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                    )
                  ]
              ),
            ),
          )),
    );
  }
}
