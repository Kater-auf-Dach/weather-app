import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'package:weather_app/screens/routes.dart';
import 'package:weather_app/screens/weather_forecast_screen.dart';
import 'package:weather_app/utils/shared_prefs.dart';
import 'package:weather_app/utils/theme_changer.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIOverlays([]);
  await sharedPrefs.init();

  // runApp(MultiProvider(
  //   providers: [
  //     ChangeNotifierProvider<ThemeChanger>(
  //       create: (_) => ThemeChanger(),
  //     )
  //   ],
  //   child: MyApp(),
  // ));

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ThemeChanger>(
      create: (BuildContext context) => ThemeChanger(),
      child: Consumer<ThemeChanger>(
        builder: (context, changer, _) {
          return MaterialApp(
            theme: changer.theme,
            home: WeatherForecastScreen(),
            routes: Routes.mainRoute,
            debugShowCheckedModeBanner: false,
          );
        },
      ),
    );
  }
}
