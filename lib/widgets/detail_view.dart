import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:weather_app/models/weather_current.dart';

class DetailView extends StatelessWidget {
  final AsyncSnapshot<WeatherCurrent> snapshot;

  DetailView({this.snapshot});

  @override
  Widget build(BuildContext context) {
    var data = snapshot.data.main;
    var temp = data.temp.toStringAsFixed(1);
    var pressure = data.pressure * 0.750062;
    var humidity = data.humidity;
    var wind = snapshot.data.wind.speed;
    TimeOfDay now = TimeOfDay.now();
    print(now);

    return Column(
      children: [
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                SvgPicture.asset(
                  "assets/images/pressure.svg",
                  color: Theme.of(context).accentColor,
                  height: 18,
                  width: 18,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Text('${pressure.round()}',
                      style: TextStyle(fontSize: 24, color: Theme.of(context).accentColor)),
                ),
              ],
            ),
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Text('/',
                      style: TextStyle(fontSize: 24, color: Theme.of(context).accentColor)),
                ),
                Text('$temp °',
                    style: TextStyle(fontSize: 42, fontWeight: FontWeight.bold, color: Theme.of(context).primaryColor)),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Text('/',
                      style: TextStyle(fontSize: 24, color: Theme.of(context).accentColor)),
                ),
              ],
            ),
            Row(
              children: [
                SvgPicture.asset(
                  "assets/images/humidity.svg",
                  color: Theme.of(context).accentColor,
                  height: 18,
                  width: 18,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Text('$humidity',
                      style: TextStyle(fontSize: 24, color: Theme.of(context).accentColor)),
                )
              ],
            ),
          ],
        ),
        SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              "assets/images/wind.svg",
              color: Theme.of(context).accentColor,
              height: 18,
              width: 18,
            ),
            Padding(
                padding: const EdgeInsets.only(left: 8),
                child: Text(
                  '${wind.toInt()} м/сек',
                  style: TextStyle(fontSize: 18, color: Theme.of(context).accentColor),
                )),
          ],
        ),
      ],
    );
  }
}
