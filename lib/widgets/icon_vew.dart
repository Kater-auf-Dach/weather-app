import 'package:flutter/material.dart';
import 'package:weather_app/models/weather_current.dart';

class IconView extends StatelessWidget {
  final AsyncSnapshot<WeatherCurrent> snapshot;

  IconView({this.snapshot});

  @override
  Widget build(BuildContext context) {
    var icon = snapshot.data.weather[0].getIcon();

    // TODO: выравнивание
    // return Center(
    //   child: Icon(
    //     icon,
    //     color: Theme.of(context).primaryColor,
    //     size: 120,
    //   ),
    // );

    return SizedBox(
      width: double.infinity,
      child: Icon(
        icon,
        color: Theme.of(context).primaryColor,
        size: 140,
      ),
    );
  }
}
