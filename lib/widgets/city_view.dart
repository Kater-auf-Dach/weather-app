import 'package:flutter/material.dart';
import 'package:weather_app/models/weather_current.dart';

class CityView extends StatelessWidget {
  final AsyncSnapshot<WeatherCurrent> snapshot;

  const CityView({this.snapshot});

  @override
  Widget build(BuildContext context) {
    var city = snapshot.data.name;
    var description = snapshot.data.weather[0].description.toUpperCase();

    return Column(
      children: [
      GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, '/city');
        },
        child: Text(
          '$city',
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 28.0,
              color: Theme.of(context).primaryColor),
        )
      ),
        Padding(padding: EdgeInsets.only(bottom: 20)),
        Text(
          '$description',
          style: TextStyle(fontSize: 18, color: Theme.of(context).accentColor),
        ),
      ],
    );
  }
}
