import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:weather_app/models/weather_current.dart';
import 'package:weather_app/utils/constants.dart';

import 'http_exception.dart';

// TODO: обработка ошибок
class WeatherApi {
  static const baseUrl = Constants.WEATHER_BASE_URL;
  final apiKey = Constants.WEATHER_APP_ID;

  Future<WeatherCurrent> fetchWeatherWithCity({String cityName}) async {
    var parameters = {
      "q": cityName,
      "units": "metric",
      "lang": "ru",
      "appid": apiKey,
    };

    return await _fetchWeather(parameters);
  }

  Future<WeatherCurrent> fetchWeatherWithLocation(
      {String latitude, String longitude}) async {
    var parameters = {
      "lat": latitude,
      "lon": longitude,
      "units": "metric",
      "lang": "ru",
      "appid": apiKey,
    };

    return await _fetchWeather(parameters);
  }

  Future<String> getCityNameFromLocation(
      {String latitude, String longitude}) async {
    var parameters = {
      "lat": latitude,
      "lon": longitude,
      "appid": apiKey,
    };

    final uri = Uri.https(Constants.WEATHER_BASE_URL,
        Constants.WEATHER_FORECAST_PATH, parameters);

    final response = await http.get(uri);

    print(response);

    if (response.statusCode != 200) {
      throw HTTPException(response.statusCode, "unable to fetch weather data");
    }

    final weatherJson = json.decode(response.body);
    return weatherJson['name'];
  }

  Future<WeatherCurrent> _fetchWeather(Map<String, String> parameters) async {
    final uri = Uri.https(Constants.WEATHER_BASE_URL,
        Constants.WEATHER_FORECAST_PATH, parameters);

    print(uri);

    final response = await http.get(uri);

    if (response.statusCode == 200) {
      return WeatherCurrent.fromJson(json.decode(response.body));
    } else {
      return null;
      // throw HTTPException(response.statusCode, "unable to fetch weather data");
    }
  }
}
