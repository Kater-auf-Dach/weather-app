import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:weather_app/models/address-result.dart';
import 'package:weather_app/utils/constants.dart';

import 'http_exception.dart';

class AddressApi {

  Future<List<AddressResult>> getCities(String value) async {
    var parameters = {
      "query": value,
      "contentType": "city",
    };

    final uri = Uri.https(Constants.ADDRESS_BASE_URL, Constants.ADDRESS_PATH, parameters);

    print(uri);

    print('fetching $uri');

    final response = await http.get(uri);

    if (response.statusCode != 200) {
      throw HTTPException(response.statusCode, "unable to fetch weather data");
    }

    return AddressResponse.fromJson(json.decode(response.body)).result;
  }

}