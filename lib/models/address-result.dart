class AddressResponse {
  SearchContext searchContext;
  List<AddressResult> result;

  AddressResponse({this.searchContext, this.result});

  AddressResponse.fromJson(Map<String, dynamic> json) {
    searchContext = json['searchContext'] != null
        ? new SearchContext.fromJson(json['searchContext'])
        : null;
    if (json['result'] != null) {
      result = new List<AddressResult>();
      json['result'].forEach((v) {
        result.add(new AddressResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.searchContext != null) {
      data['searchContext'] = this.searchContext.toJson();
    }
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SearchContext {
  String contentType;
  String query;

  SearchContext({this.contentType, this.query});

  SearchContext.fromJson(Map<String, dynamic> json) {
    contentType = json['contentType'];
    query = json['query'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['contentType'] = this.contentType;
    data['query'] = this.query;
    return data;
  }
}

class AddressResult {
  String id;
  String name;
  String zip;
  String type;
  String typeShort;
  String okato;
  String contentType;
  String guid;
  String ifnsfl;
  String ifnsul;
  String oktmo;
  String parentGuid;
  String cadnum;

  AddressResult(
      {this.id,
        this.name,
        this.zip,
        this.type,
        this.typeShort,
        this.okato,
        this.contentType,
        this.guid,
        this.ifnsfl,
        this.ifnsul,
        this.oktmo,
        this.parentGuid,
        this.cadnum});

  AddressResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    zip = json['zip'].toString();
    type = json['type'];
    typeShort = json['typeShort'];
    okato = json['okato'];
    contentType = json['contentType'];
    guid = json['guid'];
    ifnsfl = json['ifnsfl'];
    ifnsul = json['ifnsul'];
    oktmo = json['oktmo'];
    parentGuid = json['parentGuid'];
    cadnum = json['cadnum'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['zip'] = this.zip;
    data['type'] = this.type;
    data['typeShort'] = this.typeShort;
    data['okato'] = this.okato;
    data['contentType'] = this.contentType;
    data['guid'] = this.guid;
    data['ifnsfl'] = this.ifnsfl;
    data['ifnsul'] = this.ifnsul;
    data['oktmo'] = this.oktmo;
    data['parentGuid'] = this.parentGuid;
    data['cadnum'] = this.cadnum;
    return data;
  }
}