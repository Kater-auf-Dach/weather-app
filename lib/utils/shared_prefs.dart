import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_app/utils/constants.dart';

class SharedPrefs {
  static SharedPreferences _sharedPrefs;

  init() async {
    if (_sharedPrefs == null) {
      _sharedPrefs = await SharedPreferences.getInstance();
    }
  }

  // City name
  String get cityName => _sharedPrefs.getString(Constants.kCityName) ?? "";

  set cityName(String value) {
    _sharedPrefs.setString(Constants.kCityName, value);
  }

  // Theme
  String get theme => _sharedPrefs.getString(Constants.kTheme) ?? ThemesTypes.auto.inString;

  set theme(String value) {
    _sharedPrefs.setString(Constants.kTheme, value);
  }
}

final sharedPrefs = SharedPrefs();