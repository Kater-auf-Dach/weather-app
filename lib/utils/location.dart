import 'package:geolocator/geolocator.dart';

class Location {
  Future<Position> getLastKnownLocation() async {
    var granted = await _checkPermissions();

    if (granted) {
      Position lastKnownPosition = await Geolocator.getLastKnownPosition();

      if (lastKnownPosition != null) {
        return lastKnownPosition;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  Future<Position> getCurrentLocation() async {
    var granted = await _checkPermissions();
    var enabled = await _checkLocationServiceEnabled();

    if (granted && enabled) {
      Position position = await Geolocator.getCurrentPosition();

      if (position != null) {

        return position;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  Future<bool> _checkPermissions() async {
    LocationPermission permission = await Geolocator.requestPermission();

    if (permission == LocationPermission.always ||
        permission == LocationPermission.whileInUse) {
      return true;
    }

    return false;
  }

  Future<bool> _checkLocationServiceEnabled() async {
    if (await Geolocator.isLocationServiceEnabled()) {
      return true;
    }

    await Geolocator.openLocationSettings();
    if (await Geolocator.isLocationServiceEnabled()) {
      return true;
    }

    return false;
  }
}