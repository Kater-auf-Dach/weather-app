import 'dart:async';

import 'package:flutter/material.dart';
import 'package:weather_app/utils/shared_prefs.dart';
import 'package:weather_app/ui/theme.dart';

import 'constants.dart';

// TODO: Заменить на константы/enum
class ThemeChanger with ChangeNotifier {
  ThemeData _selectedTheme;
  String _selectedThemeName;
  var _timer;

  ThemeChanger() {
    _selectTheme();
  }

  ThemeData get theme => _selectedTheme;

  String get themeName => _selectedThemeName;

  setTheme(themeName) async {
    if (themeName == 'dark') {
      _setDarkTheme();
    } else if (themeName == 'light') {
      _setLightTheme();
    } else {
      _setAutoTheme();
    }

    notifyListeners();
  }

  toggleTheme() {
    if (themeName == 'light') {
      setTheme('dark');
    } else if (themeName == 'dark') {
      setTheme('auto');
    } else {
      if (_timer != null) {
        _timer.cancel();
      }
      setTheme('light');
    }
  }

  _selectTheme() {
    String theme = sharedPrefs.theme ?? 'light';
    setTheme(theme);
  }

  _setDarkTheme() {
    _selectedTheme = darkTheme();
    _selectedThemeName = 'dark';
    sharedPrefs.theme = 'dark';
  }

  _setLightTheme() {
    _selectedTheme = lightTheme();
    _selectedThemeName = 'light';
    sharedPrefs.theme = 'light';
  }

  _setAutoTheme() {
    sharedPrefs.theme = 'auto';

    DateTime now = DateTime.now();
    DateTime darkThemeStartTime = DateTime(now.year, now.month, now.day, 19);
    DateTime darkThemeEndTime = DateTime(now.year, now.month, now.day, 7);

    int timerSeconds;

    if (now.compareTo(darkThemeEndTime) > 0 &&
        now.compareTo(darkThemeStartTime) < 0) {
      _selectedTheme = lightTheme();
      _selectedThemeName = 'auto-light';
      sharedPrefs.theme = 'auto-light';

      timerSeconds = darkThemeStartTime.difference(now).inSeconds;
    } else if (now.compareTo(darkThemeStartTime) > 0) {
      _selectedTheme = darkTheme();
      _selectedThemeName = 'auto-dark';
      sharedPrefs.theme = 'auto-dark';

      DateTime nextDayMorningTime = darkThemeStartTime.add(Duration(days: 1));
      timerSeconds = nextDayMorningTime.difference(now).inSeconds;
    } else {
      _selectedTheme = darkTheme();
      _selectedThemeName = 'auto-dark';
      sharedPrefs.theme = 'auto-dark';

      timerSeconds = darkThemeEndTime.difference(now).inSeconds;
    }

    _timer = Timer(
      Duration(seconds: timerSeconds),
      () {
        _setAutoTheme();
      },
    );
  }
}
