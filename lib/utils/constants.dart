import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class Constants {
  static const String WEATHER_APP_ID = '0b41729c33db6d806d25498e2b80dd8d';
  static const String WEATHER_BASE_URL = 'api.openweathermap.org';
  static const String WEATHER_FORECAST_PATH = '/data/2.5/weather';
  static const String WEATHER_IMAGES_URL = 'https://openweathermap.org/img/wn/';

  static const String ADDRESS_BASE_URL = 'kladr-api.ru';
  static const String ADDRESS_PATH = '/api.php';

  static const kPrimaryColorText = Colors.black45;

  static const kCityName = 'cityName';
  static const kTheme = 'theme';
}

enum ThemesTypes {
  auto,
  dark,
  light,
}

extension ThemesTypesEx on ThemesTypes {
  String get inString => describeEnum(this);
}