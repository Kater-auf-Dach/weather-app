import 'package:flutter/material.dart';

ThemeData lightTheme() {
  final ThemeData base = ThemeData.light();

  return base.copyWith(
    backgroundColor: Colors.white,
    primaryColor: Colors.black,
    accentColor: Colors.grey[500],
  );
}

ThemeData darkTheme() {
  final ThemeData base = ThemeData.dark();

  return base.copyWith(
    backgroundColor: Colors.black,
    primaryColor: Colors.white,
    accentColor: Colors.grey[500],
  );
}